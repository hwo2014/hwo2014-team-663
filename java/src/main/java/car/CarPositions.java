package car;

import java.util.HashMap;
import java.util.Set;

import car.carposition.CarPosition;

public class CarPositions {
    
    HashMap<String, CarPosition> positions = new HashMap<String, CarPosition>();
    
    public CarPositions(CarPosition[] positions) {
        for(CarPosition cp : positions)
            add(cp);
    }
    
    public void add(CarPosition carPosition) {
        positions.put(carPosition.getId().getColor(), carPosition);
    }
    
    public CarPosition get(String color) {
        return positions.get(color);
    }
    
    public Set<String> getColors() {
        return positions.keySet();
    }

}
