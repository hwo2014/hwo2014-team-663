package car.carposition;

public class LanePosition {
    
    private int startLaneIndex, endLaneIndex;
    
    public int getStartLaneIndex() {
        return startLaneIndex;
    }
    
    public int getEndLaneIndex() {
        return endLaneIndex;
    }

}
