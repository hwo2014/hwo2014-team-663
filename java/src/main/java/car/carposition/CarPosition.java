package car.carposition;

import car.CarId;

public class CarPosition {
    
    private CarId id;
    private double angle, speed = 0;
    private PiecePosition piecePosition;
    private int tick;
    
    public CarId getId() {
        return id;
    }
    
    public double getAngle() {
        return angle;
    }
    
    public PiecePosition getPiecePosition() {
        return piecePosition;
    }
    
    public void setTick(int tick) {
        this.tick = tick;
    }
    public int getTick() {
        return tick;
    }
    
    public void setSpeed(double speed) {
        this.speed = speed;
    }
    public double getSpeed() {
        return speed;
    }
}
