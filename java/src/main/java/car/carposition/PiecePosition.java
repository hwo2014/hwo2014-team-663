package car.carposition;

public class PiecePosition {
    
    private int pieceIndex, lap;
    private double inPieceDistance;
    private LanePosition lane;
    
    public int getPieceIndex() {
        return pieceIndex;
    }
    
    public int getLap() {
        return lap;
    }
    
    public double getInPieceDistance() {
        return inPieceDistance;
    }
    
    public LanePosition getLane() {
        return lane;
    }

}
