package car.turbo;

public class TurboMessage {
    
    private int turboDurationTicks;
    private double turboFactor;
    
    public int getTurboDuration() {
        return turboDurationTicks;
    }
    
    public double getTurboFactor() {
        return turboFactor;
    }

}
