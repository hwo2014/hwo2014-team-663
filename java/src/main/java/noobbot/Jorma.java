package noobbot;

import car.carposition.CarPosition;
import model.*;

import java.io.IOException;

public class Jorma extends Driver {

    private Track track;
    private TrackSection lastTrackSection = null;
    private TrackPiece lastTrackPiece;
    private int brakeTicks = 0;
    private CarPosition lastPosition = null;
    private double speed = 0;
    private int lastTick = 0;
    private double pieceMaxSpeed = 0;
    private double angle = 0;
    private double beforePieceDistance = 0;
    private double inPieceDistance = 0;

    public Jorma(Track track) {
        super(track);
        this.track = track;
        //        lastTrackSection = track.getSection(0);
        lastTrackPiece = track.getTrackPiece(0);
    }

    public double getThrottle(CarPosition position, int tick) throws IOException {
        TrackPiece currentPiece = track.getTrackPiece(position.getPiecePosition().getPieceIndex());
        int currentLane = position.getPiecePosition().getLane().getStartLaneIndex();
        LanePiece currentLanePiece = currentPiece.getLanePiece(currentLane);

        if(inPieceDistance > position.getPiecePosition().getInPieceDistance()) { //the piece has changed
            beforePieceDistance += lastTrackPiece.getLanePiece(lastPosition.getPiecePosition().getLane().getStartLaneIndex()).getLength();
        }
        inPieceDistance = position.getPiecePosition().getInPieceDistance();

        //calculate speed, angle and the differences
        int tickDifference = (tick-lastTick);
        double oldSpeed = speed;
        double oldAngle = angle;
        calculateSpeed(position, tick);
        angle = position.getAngle();
        double acceleration = speed-oldSpeed;
        double dAngle = angle-oldAngle;

        System.out.println(acceleration + "\t" + speed + "\t" + getFullDistance() + "\t" + tickDifference);

        lastTrackPiece = currentPiece;
        double throttle = 0;
        if(getFullDistance()  < 450) {
            throttle = breakNow(400, 5);
        } else if(getFullDistance() > 450 && getFullDistance()  <= 950) {
            throttle = breakNow(900, 3);
        } else if(getFullDistance() > 950 && getFullDistance()  <= 1450) {
            throttle = breakNow(1400, 9);
        } else if(getFullDistance() > 1450 && getFullDistance()  <= 2000) {
            throttle = breakNow(1950, 5);
        }

        return throttle;

        //return currentLanePiece.getMaxThrottle();
    }

    private double getFullDistance() {
        return inPieceDistance+beforePieceDistance;
    }

    //returns the throttle
    private double breakNow(double targetDistance, double targetSpeed) {
        double distance = targetDistance - getFullDistance();
        double dSpeed = targetSpeed - speed;

        double funnyDistance = Math.abs(dSpeed * 5);

        if(funnyDistance > distance) return 1;
        //inside the target speed margin
        if(Math.abs(dSpeed) < 0.2) {
            return targetSpeed / 10;
        }

        if(funnyDistance < distance) {
            if(dSpeed < 0) return 0;
            else return 1;
        }

        return 0;
    }

    public String getSwitchLane(CarPosition position) {
        TrackPiece currentPiece = track.getTrackPiece(position.getPiecePosition().getPieceIndex());
        int currentLane = position.getPiecePosition().getLane().getEndLaneIndex();
        LanePiece currentLanePiece = currentPiece.getLanePiece(currentLane);
        TrackSection currentTrackSection = currentLanePiece.getParentSection().getParentSection();

        String switchLane = null;

        if(lastTrackSection != currentTrackSection) {

            LaneSection straight = currentTrackSection.getLaneSection(currentLane).getStraight();
            LaneSection left = currentTrackSection.getLaneSection(currentLane).getLeft();
            LaneSection right = currentTrackSection.getLaneSection(currentLane).getRight();

            if(left != null) {
                if(straight.getLength() < left.getLength()) {
                    switchLane = "Left";
                }
            } else {
                if(straight.getLength() < right.getLength()) {
                    switchLane = "Right";
                }
            }

            lastTrackSection = currentTrackSection;
        }

        return switchLane;
    }

    private void calculateSpeed(CarPosition currentPosition, int tick) {
        if(lastPosition != null) {
            if(currentPosition.getPiecePosition().getPieceIndex() == lastPosition.getPiecePosition().getPieceIndex()) {
                speed = currentPosition.getPiecePosition().getInPieceDistance() - lastPosition.getPiecePosition().getInPieceDistance();
            } else {
                TrackPiece trackPiece = track.getTrackPiece(lastPosition.getPiecePosition().getPieceIndex());
                double length = trackPiece.getLanePiece(lastPosition.getPiecePosition().getLane().getEndLaneIndex()).getLength();
                speed = currentPosition.getPiecePosition().getInPieceDistance() + (length - lastPosition.getPiecePosition().getInPieceDistance());
            }
        }
        if(tick > lastTick)
            speed = speed / (tick - lastTick);

        lastPosition = currentPosition;
        lastTick = tick;

        if(speed > pieceMaxSpeed) {
            pieceMaxSpeed = speed;
        }
    }

    private double distanceToNextCurve(CarPosition position, TrackPiece nextCurve) {
        final int lastIndex = track.getTrackPieces().size();
        int index = position.getPiecePosition().getPieceIndex();

        TrackPiece piece = track.getTrackPiece(index);
        double distance = piece.getLanePiece(position.getPiecePosition().getLane().getEndLaneIndex()).getLength() - position.getPiecePosition().getInPieceDistance();

        while(true) {
            if(++index >= lastIndex) {
                index = 0;
            }
            piece = track.getTrackPiece(index);

            if(piece.isCurve()) {
                nextCurve.setLanePieces(piece.getLanePieces());
                break;
            }

            distance += piece.getLanePiece(0).getLength();
        }

        return distance;
    }
    
    public void crash() {
        pieceMaxSpeed -= 1;
    }

}
