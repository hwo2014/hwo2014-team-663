package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.Socket;

import message.gameinit.GameInitMessage;
import model.LanePiece;
import model.Track;
import model.TrackPiece;
import car.CarId;
import car.carposition.CarPosition;
import car.turbo.TurboMessage;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        SendMsg createMsg = null;
        Join join = new Join(botName, botKey);

        if(args.length > 4) {
            String trackName = args[4];
            Join botId = join;
            createMsg = new CreateRace(trackName, botName, 1, botId);
            System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " at " + trackName);

        }
        else {
            createMsg = join;
            System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " at finland?");

        }

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));


        new Main(reader, writer, createMsg);
    }

    final Gson gson;
    private PrintWriter writer;
    private Track track;
    private String carColor = "";
    private Driver driver;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        FieldNamingStrategy fieldNamingStrategy = new CustomFieldNamingStrategy();
        gsonBuilder.setFieldNamingStrategy(fieldNamingStrategy);
        gson = gsonBuilder.create();
        
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final ReceiveMessage msgFromServer = gson.fromJson(line, ReceiveMessage.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                
                CarPosition[] carPositions = gson.fromJson(msgFromServer.data, CarPosition[].class);
                
                for(int i = 0; i < carPositions.length; i++) {
                    carPositions[i].setTick(msgFromServer.gameTick);
                }
                
                driver.setCarPositions(carPositions);
                
                CarPosition currentPosition = findCarPosition(carColor, carPositions);
                
                String switchLane = driver.getSwitchLane(currentPosition);
                boolean turbo = driver.getTurbo();
                
                if(turbo) {
                    System.out.println("going turbo!");
                    send(new Turbo("GOING TURBOOoo"));
                }else if(switchLane == null) {
                    send(new Throttle(driver.getThrottle(currentPosition, msgFromServer.gameTick)));
                } else {
                    System.out.println("switching to " + switchLane + " on tick " + msgFromServer.gameTick);
                    send(new SwitchLane(switchLane));
                }
                
                
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("yourCar")) {
                CarId carId = gson.fromJson(msgFromServer.data, CarId.class);
                carColor = carId.getColor();
                System.out.println("Our car color is " + carColor);
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                initRace(msgFromServer);
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                TrackBuilder.printLanePieces(track);
                System.out.println("Race end");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Ping());
            } else {
                send(new Ping());
            }
            if (msgFromServer.msgType.equals("crash")) {
                driver.crash();
            }
            if (msgFromServer.msgType.equals("spawn")) {
                System.out.println("SPAWN!");
            }
            if (msgFromServer.msgType.equals("turboAvailable")) {
                TurboMessage turbo = gson.fromJson(msgFromServer.data, TurboMessage.class);
                driver.setTurbo(turbo);
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    private void initRace(ReceiveMessage message) {
        GameInitMessage data = gson.fromJson(message.data, GameInitMessage.class);
        track = TrackBuilder.buildTrack(data.race.track);
        driver = new Driver(track);
        driver.setColor(carColor);
    }
    
    private CarPosition findCarPosition(String color, CarPosition[] positions) {
        
        for(int i = 0; i < positions.length; i++) {
            if(color.equals(positions[i].getId().getColor())) {
                return positions[i];
            }
        }
        
        System.out.println("Failed to find own car in carPositions!");
        return null;
    }
}

class ReceiveMessage {
    public String msgType;
    public JsonElement data;
    public int gameTick;
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class CreateRace extends SendMsg {
    public final String trackName;
    public final String password;
    public final int carCount;

    public final Join botId;

    CreateRace(final String trackName, final String password, final int carCount, final Join botId) {
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;

        this.botId = botId;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
    
}

class Turbo extends SendMsg {
    private String value;
    
    public Turbo(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }
    
    @Override
    protected String msgType() {
        return "turbo";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
    
}

class CustomFieldNamingStrategy implements FieldNamingStrategy {
    @Override
    public String translateName(Field field) {
        
        if("isSwitch".equals(field.getName())) {
            return "switch";
        }
        
        return field.getName();
    }
}