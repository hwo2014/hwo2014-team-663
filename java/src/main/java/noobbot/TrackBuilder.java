package noobbot;

import message.gameinit.LaneData;
import message.gameinit.PieceData;
import message.gameinit.TrackData;
import model.Curve;
import model.Lane;
import model.LanePiece;
import model.LaneSection;
import model.Track;
import model.TrackPiece;
import model.TrackSection;

public class TrackBuilder {
    
    public static Track buildTrack(TrackData trackData) {
        
        Track track = new Track();
        
        for(LaneData ld : trackData.lanes) {
            track.addLane(new Lane(ld.index, ld.distanceFromCenter));
        }
        
        int totalLanes = track.numberOfLanes();
        TrackSection trackSection = new TrackSection(totalLanes);
        
        int index = 0;
        
        for(PieceData pd : trackData.pieces) {
            
            if(pd.isSwitch) {
                track.addSection(trackSection);
                TrackSection tmpSection = new TrackSection(totalLanes);
                setFollowingLaneSections(trackSection, tmpSection);
                trackSection = tmpSection;
            }
            
            LanePiece[] lanePieces = new LanePiece[totalLanes];
            
            for(int i = 0; i < totalLanes; i++) {
                LaneSection laneSection = trackSection.getLaneSection(i);
                LanePiece lanePiece = pd.length > 0 ? new LanePiece(laneSection, pd.length) : new LanePiece(laneSection, pd.radius, pd.angle, track.getLane(i).getDistanceFromCenter());
                lanePiece.setIndex(index);
                lanePiece.setLaneIndex(i);
                laneSection.addPiece(lanePiece);
                lanePieces[i] = lanePiece;
            }
            
            track.addPiece(new TrackPiece(index++, pd.isSwitch, pd.radius > 0, lanePieces));
        }
        if(track.getTrackPiece(0).isSwitch()) {
            track.addSection(trackSection);
            setFollowingLaneSections(trackSection, track.getSection(0));
        } else {
            joinLaneSections(trackSection, track.getSection(0));
            setFollowingLaneSections(track.getSection(track.getSections().size() - 1), track.getSection(0));
        }
        
        for(int i = 0; i < totalLanes; i++) {
            
            Curve curve = new Curve();
            for(TrackPiece trackPiece : track.getTrackPieces()) {
                LanePiece lanePiece = trackPiece.getLanePiece(i);
                
                if(!lanePiece.isCurve()) {
                    curve = new Curve();
                    continue;
                }
                if(trackPiece.getIndex() > 0) {
                    TrackPiece prevTrackPiece = track.getTrackPiece(trackPiece.getIndex() - 1);
                    LanePiece prevLanePiece = prevTrackPiece.getLanePiece(i);
                    if((lanePiece.getAngle() > 0 && prevLanePiece.getAngle() < 0) || (lanePiece.getAngle() < 0 && prevLanePiece.getAngle() > 0)) {
                        curve = new Curve();
                        curve.addLanePiece(lanePiece);
                        continue;
                    }
                }
                curve.addLanePiece(lanePiece);
            }
        }
        
        Calibrator.initialize(track);
        
        printTrack(track);
        
        return track;
    }
    
    private static void setFollowingLaneSections(TrackSection firstSection, TrackSection secondSection) {
        LaneSection[] laneSections = firstSection.getLaneSections();
        
        for(int i = 0; i < laneSections.length; i++) {
            laneSections[i].setStraight(secondSection.getLaneSection(i));
            if(i > 0) {
                laneSections[i].setLeft(secondSection.getLaneSection(i-1));
            }
            if(i < laneSections.length - 1) {
                laneSections[i].setRight(secondSection.getLaneSection(i+1));
            }
        }
    }
    
    //otetaan ensimm�isen sectionin palikat ja lis�t��n ne toisen sectionin alkuun
    private static void joinLaneSections(TrackSection firstSection, TrackSection secondSection) {
        for(int i = 0; i < secondSection.getLaneSections().length; i++) {
            LaneSection firstLaneSection = firstSection.getLaneSection(i);
            LaneSection secondLaneSection = secondSection.getLaneSection(i);
            for(int j = firstLaneSection.getPieces().size() - 1; j >= 0; j--) {
                LanePiece lp = firstLaneSection.getPiece(j);
                lp.setParent(secondLaneSection);
                secondLaneSection.addPieceFirst(lp);
            }
        }
    }
    
    public static void printTrack(Track track) {
        
        String fileName = "track.txt";
        
        for(TrackPiece trackPiece : track.getTrackPieces()) {
            Util.printToFile(fileName, trackPiece);
            for(LanePiece lanePiece : trackPiece.getLanePieces()) {
                Util.printToFile(fileName, lanePiece);
            }
        }
        for(TrackSection ts : track.getSections()) {
            for(LaneSection ls : ts.getLaneSections()) {
                Util.printToFile(fileName, ls);
            }
        }
    }
    
    public static void printLanePieces(Track track) {
        
        String fileName = "lanepieces.txt";
        
        Util.printToFile(fileName, "track");
        
        for(TrackPiece trackPiece : track.getTrackPieces()) {
            for(LanePiece lanePiece : trackPiece.getLanePieces()) {
                Util.printToFile(fileName, lanePiece);
            }
        }
    }

}
