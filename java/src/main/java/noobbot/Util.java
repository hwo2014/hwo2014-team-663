package noobbot;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;

import car.carposition.CarPosition;
import model.LanePiece;
import model.TrackPiece;

public class Util {

    public static void printToFile(String fileName, Object line) {
        try {
            FileWriter output;
            output = new FileWriter(fileName, true);
            PrintWriter printer = new PrintWriter(output);
            printer.println(line);
            printer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double median(double[] values) {

        if(values.length == 1) {
            return values[0];
        }

        Arrays.sort(values);
        double median = 0;

        int middle = ((values.length) / 2);
        if(values.length % 2 == 0){
            double medianA = values[middle];
            double medianB = values[middle-1];
            median = (medianA + medianB) / 2d;
        } else{
            median = values[middle + 1];
        }

        return median;
    }
    
    public static double mean(Collection<Double> values) {
        
        double sum = 0;
        
        for(double d : values) {
            sum += d;
        }
        return sum / values.size();
    }

}
