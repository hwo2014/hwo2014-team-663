package noobbot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import model.LanePiece;
import model.LaneSection;
import model.Track;
import model.TrackPiece;
import model.TrackSection;
import car.CarPositions;
import car.carposition.CarPosition;
import car.turbo.TurboMessage;

public class Driver {

    private final int POSITIONHISTORYSIZE = 3;
    private final double SPEEDTHRESHOLD = 0.15;
    
    String carColor = "red";

    private Track track;
    private TrackSection lastTrackSection = null;
    private TrackPiece lastTrackPiece;
    private LanePiece lastLanePiece;
    
    private int brakeTicks = 0;
    private LinkedList<CarPositions> lastPositions = new LinkedList<CarPositions>();
    private double speed = 0;
    private double distanceTraveled = 0, brakeStartMarker = 0, brakeStartSpeed = 0;
    private double pieceMaxSpeed = 0, maxSlideAngle = 0;
    private boolean crashed = false;
    private boolean switchMessageSent = false, brakingStarted = false, brakingPrinted = false;
    
    private double brakeDistanceMultiplier = 70;
    private LinkedList<Double> bdMultipliers = new LinkedList<Double>();

//    private LaneRoutePicker routePicker = new ShortestRoutePicker();
    private LaneRoutePicker routePicker = new LongestRoutePicker();
//    private LaneRoutePicker routePicker = new StraightPicker();
    
    private Calibrator throttlePlan = new Calibrator();
    
    private int turboRemaining = 0;
    private double turboFactor = 1;

    public Driver(Track track) {
        this.track = track;
        //        lastTrackSection = track.getSection(0);
        lastTrackPiece = track.getTrackPiece(0);
        lastLanePiece = lastTrackPiece.getLanePiece(0);
    }
    
    public void setColor(String color) {
        carColor = color;
    }

    public double getThrottle(CarPosition position, int tick) throws IOException {

        TrackPiece currentPiece = track.getTrackPiece(position.getPiecePosition().getPieceIndex());
        int currentLane = position.getPiecePosition().getLane().getStartLaneIndex();
        LanePiece currentLanePiece = currentPiece.getLanePiece(currentLane);
        
        TrackPiece nextPiece = track.getTrackPiece(position.getPiecePosition().getPieceIndex() + 1);
        LanePiece nextLane = nextPiece.getLanePiece(currentLane);
        
        DrivePlan drivePlan = getDrivePlan(position, 500);

        if(currentPiece != lastTrackPiece) {
            
            throttlePlan.calibrate(lastLanePiece, maxSlideAngle, crashed);
            crashed = false;
            maxSlideAngle = 0;
            
            for(LanePiece lp : drivePlan.getPieces()) {
                    Util.printToFile("driveplan.txt", lp);
            }
            Util.printToFile("driveplan.txt", "");

            if(position.getPiecePosition().getLap() == 0) {
//                lastTrackPiece.getLanePiece(currentLane).setMaxThrottle(pieceMaxSpeed/10.0);
            }
            pieceMaxSpeed = 0;
            
            if(lastTrackPiece.getLanePiece(0).getParentSection() != currentPiece.getLanePiece(0).getParentSection()) {
                switchMessageSent = false;
            }

            lastTrackPiece = currentPiece;
            lastLanePiece = currentLanePiece;
        }
        
        double currentAngle = Math.abs(position.getAngle());
        
        if(currentAngle > maxSlideAngle) {
            maxSlideAngle = currentAngle;
        }
        
        double targetThrottle = 1.0, minDistanceToBrakeStart = 500;
        
        for(int i = 0; i < drivePlan.size(); i++) {
            LanePiece planPiece = drivePlan.getPiece(i);
            double distanceToBrakeStart = drivePlan.getDistanceTo(i) - calculateBrakeDistance(speed, planPiece.getMaxThrottle()*10);
            if(minDistanceToBrakeStart > distanceToBrakeStart) {
                targetThrottle = planPiece.getMaxThrottle();
                minDistanceToBrakeStart = distanceToBrakeStart;
            }
        }
        
//        System.out.println(speed + " " + targetThrottle*10 + " " + minDistanceToBrakeStart);
        
//        System.out.println(speed + " " + targetSpeed);
        if(minDistanceToBrakeStart <= 0) {
            if(speed - targetThrottle*10 > SPEEDTHRESHOLD) {
                
                if(!brakingStarted) {
                    brakingStarted = true;
                    brakingPrinted = false;
                    brakeStartMarker = distanceTraveled;
                    brakeStartSpeed = speed;
                }
                
                return 0;
            } else if (speed - targetThrottle*10 < -SPEEDTHRESHOLD) {
                return 1;
            } else {
                if(!brakingPrinted) {
                    Util.printToFile("brakes", brakeStartSpeed + "\t" + targetThrottle*10 + "\t" + calculateBrakeDistance(brakeStartSpeed, targetThrottle*10) + "\t" + (distanceTraveled - brakeStartMarker));
                    brakingPrinted = true;
                    brakingStarted = false;
                    
                    calibrateBrakeDistance(calculateBrakeDistance(brakeStartSpeed, targetThrottle*10), distanceTraveled - brakeStartMarker);
                    System.out.println("Calibrated brakeDistance: " + brakeDistanceMultiplier);
                }
                return targetThrottle;
            }
        }
        return 1;


        /*if(position.getPiecePosition().getLap() == 0 && (position.getAngle() > 40 || position.getAngle() < -40)) {
            System.out.println("JARRUA");
//            printer.println(speed + "\t" + position.getAngle() + "\t JARRUA");
//            printer.close();
            return currentLanePiece.getMaxThrottle()/1.5;
        } else {*/

//        TrackPiece nextCurve = new TrackPiece();
//        double distance = distanceToNextCurve(position,nextCurve);
//        double minThrottle = 1.0;
//        for(LanePiece lp : nextCurve.getLanePieces()) {
//            if(minThrottle > lp.getMaxThrottle())
//                minThrottle = lp.getMaxThrottle();
//        }
//
//        if(distance < (speed - minThrottle*10) * 90) {
//            return minThrottle;
//        }
        //}

//        return currentLanePiece.getMaxThrottle();
    }

    public String getSwitchLane(CarPosition position) {
        
        if(!switchMessageSent) {
            switchMessageSent = true;
            
            LaneSection currentLaneSection = getLanePiece(position).getParentSection();
            LaneSection nextLaneSection = routePicker.pickNextSection(currentLaneSection, lastPositions.peekLast(), carColor);
            
            int currentLane = position.getPiecePosition().getLane().getEndLaneIndex();
            int nextLane = nextLaneSection.getLaneIndex();
            
            if(currentLane > nextLane) {
                return "Left";
            }
            if(currentLane < nextLane) {
                return "Right";
            }
        }
        return null;
    }
    
    public boolean getTurbo() {
        
        
        return false;
    }
    
    private void calculateSpeed(CarPosition currentPosition) {

        double curSpeed = 0;
        
        if(!lastPositions.isEmpty()) {
            CarPosition lastPosition = lastPositions.peekLast().get(currentPosition.getId().getColor());
            
            if(lastPosition == null)
                return;
            
            if(currentPosition.getPiecePosition().getPieceIndex() == lastPosition.getPiecePosition().getPieceIndex()) {
                curSpeed = currentPosition.getPiecePosition().getInPieceDistance() - lastPosition.getPiecePosition().getInPieceDistance();
            } else {
                TrackPiece trackPiece = track.getTrackPiece(lastPosition.getPiecePosition().getPieceIndex());
                double length = trackPiece.getLanePiece(lastPosition.getPiecePosition().getLane().getEndLaneIndex()).getLength();
                curSpeed = currentPosition.getPiecePosition().getInPieceDistance() + (length - lastPosition.getPiecePosition().getInPieceDistance());
            }

            if(currentPosition.getTick() != lastPosition.getTick())
                curSpeed = curSpeed / (currentPosition.getTick() - lastPosition.getTick());
        }
        
        currentPosition.setSpeed(curSpeed);
    }

    private void calculateOwnSpeed() {

        CarPosition lastPosition = lastPositions.peekLast().get(carColor);
        distanceTraveled += lastPosition.getSpeed();

        double speeds[] = new double[lastPositions.size()];
        int i = 0;
        for(CarPositions pos : lastPositions) {
            speeds[i++] = pos.get(carColor).getSpeed();
        }
        
        speed = Util.median(speeds);

        if(speed > pieceMaxSpeed) {
            pieceMaxSpeed = speed;
        }
    }

    private double distanceToNextCurve(CarPosition position, TrackPiece nextCurve) {
        final int lastIndex = track.getTrackPieces().size();
        int index = position.getPiecePosition().getPieceIndex();

        TrackPiece piece = track.getTrackPiece(index);
        double distance = piece.getLanePiece(position.getPiecePosition().getLane().getEndLaneIndex()).getLength() - position.getPiecePosition().getInPieceDistance();

        while(true) {
            if(++index >= lastIndex) {
                index = 0;
            }
            piece = track.getTrackPiece(index);

            if(piece.isCurve()) {
                nextCurve.setLanePieces(piece.getLanePieces());
                break;
            }

            distance += piece.getLanePiece(0).getLength();
        }

        return distance;
    }

    public void crash() {
        System.out.println("CRASH! speed=" + speed + " targetSpeed=" + lastLanePiece.getMaxThrottle()*10);
        pieceMaxSpeed -= 1;
        crashed = true;
    }
    
    private DrivePlan getDrivePlan(CarPosition position, double maxLength) {
        
        LanePiece lanePiece = getLanePiece(position);
        DrivePlan plan = new DrivePlan();
        double inPieceDist = position.getPiecePosition().getInPieceDistance();
        double distance =  -inPieceDist;
        
        boolean skipFirst = false;
        if(inPieceDist > lanePiece.getLength() * 9 / 10) {
//            skipFirst = true;
        }
        
        while (distance < maxLength) {
            if(!skipFirst) {
                plan.addPiece(lanePiece, distance);
                skipFirst = false;
            }
            distance += lanePiece.getLength();
            lanePiece = getNextLanePiece(lanePiece);
        }
        
        return plan;
    }

    public LanePiece getLanePiece(CarPosition position) {
        TrackPiece currentPiece = track.getTrackPiece(position.getPiecePosition().getPieceIndex());
        int currentLane = position.getPiecePosition().getLane().getEndLaneIndex();
        return currentPiece.getLanePiece(currentLane);
    }
    public LanePiece getNextLanePiece(LanePiece lanePiece) {
        TrackPiece nextTrackPiece = track.getTrackPiece(lanePiece.getIndex() + 1);
        LanePiece nextLanePiece = nextTrackPiece.getLanePiece(lanePiece.getLaneIndex());
        
        if(lanePiece.getParentSection() != nextLanePiece.getParentSection()) {
            LaneSection nextSection = routePicker.pickNextSection(lanePiece.getParentSection(), lastPositions.peekLast(), carColor);
            nextLanePiece = nextSection.getPiece(0);
        }
        
        return nextLanePiece;
    }
    
    public double calculateBrakeDistance(double speed, double targetSpeed) {
        return (speed - targetSpeed)*brakeDistanceMultiplier;
    }
    public void calibrateBrakeDistance(double estimatedBrakeDistance, double realBrakeDistance) {
        if(estimatedBrakeDistance > 0 && realBrakeDistance > 0) {
            
            bdMultipliers.offer(brakeDistanceMultiplier*realBrakeDistance/estimatedBrakeDistance);
            if(bdMultipliers.size() > 5) {
                bdMultipliers.poll();
            }
            
            brakeDistanceMultiplier = Util.mean(bdMultipliers);
        }
    }
    
    public void setTurbo(TurboMessage turbo) {
        turboRemaining = turbo.getTurboDuration();
        turboFactor = turbo.getTurboFactor();
        System.out.println("Got turbo! " + turboRemaining);
    }
    
    public void setCarPositions(CarPosition[] positions) {
        
        for(CarPosition cp : positions) {
            calculateSpeed(cp);
        }
        
        CarPositions carPositions = new CarPositions(positions);
        lastPositions.offer(carPositions);
        if(lastPositions.size() > POSITIONHISTORYSIZE) {
            lastPositions.poll();
        }
        
        calculateOwnSpeed();
    }
    
}

interface LaneRoutePicker {
    LaneSection pickNextSection(LaneSection currentSection, CarPositions carPositions, String ownColor);
}

class ShortestRoutePicker implements LaneRoutePicker {

    @Override
    public LaneSection pickNextSection(LaneSection currentSection, CarPositions carPositions, String ownColor) {
        
        LaneSection straight = currentSection.getStraight();
        LaneSection left = currentSection.getLeft();
        LaneSection right = currentSection.getRight();

        double leftLength = left == null ? 10000 : left.getLength();
        double rightLength = right == null ? 10000 : right.getLength();
        double straightLength = straight == null ? 10000 : straight.getLength();

        if(leftLength < rightLength && leftLength < straightLength) {
            return left;
        } else if(rightLength < leftLength && rightLength < straightLength) {
            return right;
        }
        return straight;
    }
}

class LongestRoutePicker implements LaneRoutePicker {

    @Override
    public LaneSection pickNextSection(LaneSection currentSection, CarPositions carPositions, String ownColor) {
        
        LaneSection straight = currentSection.getStraight();
        LaneSection left = currentSection.getLeft();
        LaneSection right = currentSection.getRight();

        double leftLength = left == null ? -1 : left.getLength();
        double rightLength = right == null ? -1 : right.getLength();
        double straightLength = straight == null ? -1 : straight.getLength();

        if(leftLength > rightLength && leftLength > straightLength) {
            return left;
        } else if(rightLength > leftLength && rightLength > straightLength) {
            return right;
        }
        return straight;
    }
}

//HETEROVALITSIN LOLOLO
class StraightPicker implements LaneRoutePicker {

    @Override
    public LaneSection pickNextSection(LaneSection currentSection, CarPositions carPositions, String ownColor) {
        return currentSection.getStraight();
    }
}

class smartRoutePicker implements LaneRoutePicker {

    @Override
    public LaneSection pickNextSection(LaneSection currentSection, CarPositions carPositions, String ownColor) {
        
        LaneSection shortest = pickShortestRoute(currentSection);
        
        ArrayList<CarPosition> enemyCars = new ArrayList<CarPosition>();
        for(String color : carPositions.getColors()) {
            if(!ownColor.equals(color))
                enemyCars.add(carPositions.get(color));
        }
        
        return shortest;
    }
    
    public LaneSection pickShortestRoute(LaneSection currentSection) {
        
        LaneSection straight = currentSection.getStraight();
        LaneSection left = currentSection.getLeft();
        LaneSection right = currentSection.getRight();

        double leftLength = left == null ? 10000 : left.getLength();
        double rightLength = right == null ? 10000 : right.getLength();
        double straightLength = straight == null ? 10000 : straight.getLength();

        if(leftLength < rightLength && leftLength < straightLength) {
            return left;
        } else if(rightLength < leftLength && rightLength < straightLength) {
            return right;
        }
        return straight;
    }
    
    
}
