package noobbot;

import java.util.ArrayList;

import model.LanePiece;

public class DrivePlan {

    private ArrayList<LanePiece> lanePieces = new ArrayList<LanePiece>();
    private ArrayList<Double> distances = new ArrayList<Double>();
    
    public void addPiece(LanePiece lanePiece, double distance) {
        lanePieces.add(lanePiece);
        distances.add(distance);
    }
    public LanePiece getPiece(int index) {
        return lanePieces.get(index);
    }
    public int size() {
        return lanePieces.size();
    }
    public double getDistanceTo(int index) {
        return distances.get(index);
    }
    public ArrayList<LanePiece> getPieces() {
        return lanePieces;
    }

}
