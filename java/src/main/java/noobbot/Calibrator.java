package noobbot;

import model.Curve;
import model.LanePiece;
import model.Track;
import model.TrackPiece;

public class Calibrator {
    
    static double targetSlideAngle = 30;
    
    public static void initialize(Track track) {
        for(TrackPiece tp : track.getTrackPieces()) {
            for(LanePiece lp : tp.getLanePieces()) {
                initialize(lp);
            }
        }
    }
    
    public static void initialize(LanePiece lanePiece) {
        if(!lanePiece.isCurve()) {
            lanePiece.setMaxThrottle(1.0);
            return;
        }
        
        double radius = lanePiece.getRadius();
        Curve curve = lanePiece.getParentCurve();
        double angle = Math.abs(curve.getTotalLength(lanePiece.getCurveIndex()));
        

//      double maxThrottle = 0.4 - 0.5 * (40 - 60*radius/angle)/160;
//      double maxThrottle = 0.65 - 0.4 * (1 - radius/angle);
      double maxThrottle = 1 - 0.4 * (angle/radius);
        if(maxThrottle > 1) {
            maxThrottle = 1;
        }
        if(maxThrottle < 0.2) {
            maxThrottle = 0.2;
        }
        
        System.out.println("Initializing: " + lanePiece.getCurveIndex() + " " + radius + " " + angle + " -> " + maxThrottle);
        
        lanePiece.setMaxThrottle(maxThrottle);
    }
    
    public static void calibrate(LanePiece lanePiece, double maxSlideAngle, boolean crashed) {
        
        if(!lanePiece.isCurve()) {
            return;
        }
        
        //everything went better than expected
        if(!crashed && maxSlideAngle > targetSlideAngle) {
            return;
        }
        
        if(crashed && maxSlideAngle < targetSlideAngle) {
            setTargetSlideAngle(targetSlideAngle - 10);
        }
        
        if(crashed) {
            maxSlideAngle = 200;
        }
        
        double oldValue = lanePiece.getMaxThrottle();
        double newValue = oldValue + 0.1 * (1 - maxSlideAngle/targetSlideAngle);
        if(newValue > 1) {
            newValue = 1;
        }
        if(newValue < 0.2) {
            newValue = 0.2;
        }
        lanePiece.setMaxThrottle(newValue);
        
        System.out.println("Calibrated LanePiece: " + maxSlideAngle + ", " + oldValue + " -> " + newValue);
    }
    
    private static void setTargetSlideAngle(double newAngle) {
        
        if(newAngle < 10) {
            newAngle = 10;
        }
        targetSlideAngle = newAngle;
    }
}
