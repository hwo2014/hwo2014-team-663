package model;


public class TrackSection {
    
    private LaneSection[] laneSections;
    
    public TrackSection(int width) {
        laneSections = new LaneSection[width];
        for(int i = 0; i < laneSections.length; i++) {
            laneSections[i] = new LaneSection(this, i);
        }
    }
    
    public LaneSection getLaneSection(int lane) {
        return laneSections[lane];
    }
    
    public LaneSection[] getLaneSections() {
        return laneSections;
    }

}
