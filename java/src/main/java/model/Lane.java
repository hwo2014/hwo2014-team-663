package model;

public class Lane {
    
    private int index;
    private int distanceFromCenter;
    
    public Lane(int index, int distanceFromCenter) {
        this.index = index;
        this.distanceFromCenter = distanceFromCenter;
    }
    
    public int getIndex() {
        return index;
    }
    
    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

}
