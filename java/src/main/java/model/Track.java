package model;

import java.util.ArrayList;

public class Track {
    
    private ArrayList<TrackPiece> trackPieces = new ArrayList<TrackPiece>();
    private ArrayList<Lane> lanes = new ArrayList<Lane>();
    private ArrayList<TrackSection> trackSections = new ArrayList<TrackSection>();
    
    public void addLane(Lane lane) {
        lanes.add(lane.getIndex(), lane);
    }
    
    public void addPiece(TrackPiece trackPiece) {
        trackPieces.add(trackPiece);
    }
    
    public void addSection(TrackSection section) {
        trackSections.add(section);
    }
    
    public TrackPiece getTrackPiece(int index) {
        return trackPieces.get(index % trackPieces.size());
    }
    
    public Lane getLane(int index) {
        return lanes.get(index);
    }
    
    public TrackSection getSection(int index) {
        return trackSections.get(index);
    }
    
    public int numberOfLanes() {
        return lanes.size();
    }
    
    public int trackLength() {
        return trackPieces.size();
    }
    
    public ArrayList<TrackPiece> getTrackPieces() {
        return trackPieces;
    }
    
    public ArrayList<TrackSection> getSections() {
        return trackSections;
    }

}
