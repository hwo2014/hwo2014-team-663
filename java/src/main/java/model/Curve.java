package model;

import java.util.ArrayList;

public class Curve {

    private ArrayList<LanePiece> pieces = new ArrayList<LanePiece>();
    private ArrayList<Double> combinedAngles = new ArrayList<Double>();
    
    public void addLanePiece(LanePiece piece) {
        piece.setCurveIndex(pieces.size());
        piece.setParentCurve(this);
        pieces.add(piece);
        if(combinedAngles.isEmpty()) {
            combinedAngles.add(piece.getAngle());
        } else {
            combinedAngles.add(combinedAngles.get(combinedAngles.size() - 1) + piece.getAngle());
        }
    }
    
    public ArrayList<LanePiece> getPieces() {
        return pieces;
    }
    
    public double getTotalLength(int index) {
        return combinedAngles.get(index);
    }
    
    public double getTotalLength() {
        return combinedAngles.get(combinedAngles.size() - 1);
    }

}
