package model;

import java.util.LinkedList;

import noobbot.Util;

public class LanePiece {

    private double length, estimatedTime = 0, maxThrottle = 0, radius = 0, angle = 0;
    private LaneSection parentSection;
    private boolean isCurve = false;
    private int index = -1, laneIndex = -1, curveIndex = -1;
    private Curve parentCurve;
    private LinkedList<Double> maxThrottles = new LinkedList<Double>();

    public LanePiece(LaneSection parentSection, double length) {
        this.parentSection = parentSection;
        this.length = length;
//        maxThrottle = 1;
    }

    public LanePiece(LaneSection parentSection, double radius, double angle, double distanceFromCenter) {
        this.parentSection = parentSection;
        double laneDistance = 0;
        if(angle > 0) {
            laneDistance = radius - distanceFromCenter;
            length = laneDistance * angle/180.0 * Math.PI;
        } else {
            laneDistance = radius + distanceFromCenter;
            length = laneDistance * -angle/180.0 * Math.PI;
        }

        //maxThrottle = 0.7 - 0.2 * (200 - radius)/100;
        isCurve = true;
        this.radius = laneDistance;
        this.angle = angle;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getLength() {
        return length;
    }

    public double getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(double estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public double getMaxThrottle() {
        return maxThrottle;
    }

    public void setMaxThrottle(double maxThrottle) {
        maxThrottles.offer(maxThrottle);
        if(maxThrottles.size() > 5) {
            maxThrottles.poll();
        }
        this.maxThrottle = Util.mean(maxThrottles);
    }
    
    public LaneSection getParentSection() {
        return parentSection;
    }
    
    public boolean isCurve() {
        return isCurve;
    }
    
    public String toString() {
//      return "LanePiece: length=" + length + " angle=" + angle + " radius=" + radius + " maxThrottle=" + maxThrottle;
      double angle = parentCurve == null ? this.angle : Math.abs(parentCurve.getTotalLength(curveIndex));
      return length + "\t" + angle + "\t" + radius + "\t" + maxThrottle;
    }

    public void setIndex(int index) {
        this.index = index;
    }
    public int getIndex() {
        return index;
    }
    public void setLaneIndex(int laneIndex) {
        this.laneIndex = laneIndex;
    }
    public int getLaneIndex() {
        return laneIndex;
    }
    public void setParent(LaneSection parent) {
        parentSection = parent;
    }
    public void setParentCurve(Curve parentCurve) {
        this.parentCurve = parentCurve;
    }
    public Curve getParentCurve() {
        return parentCurve;
    }

    public int getCurveIndex() {
        return curveIndex;
    }

    public void setCurveIndex(int curveIndex) {
        this.curveIndex = curveIndex;
    }

}
