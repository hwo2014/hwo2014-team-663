package model;

import java.util.ArrayList;

public class LaneSection {
    
    private ArrayList<LanePiece> lanePieces = new ArrayList<LanePiece>();
    private double length = 0, estimatedTime = 0;
    private int laneIndex;
    
    private LaneSection left, right, straight;
    private TrackSection parentSection;
    
    public LaneSection(TrackSection parentSection, int laneIndex) {
        this.parentSection = parentSection;
        this.laneIndex = laneIndex;
    }
    
    public void addPiece(LanePiece piece) {
        lanePieces.add(piece);
        length += piece.getLength();
        estimatedTime += piece.getEstimatedTime();
    }
    
    public void addPieceFirst(LanePiece piece) {
        lanePieces.add(0, piece);
        length += piece.getLength();
        estimatedTime += piece.getEstimatedTime();
    }
    
    public LanePiece getPiece(int index) {
        return lanePieces.get(index);
    }
    public ArrayList<LanePiece> getPieces() {
        return lanePieces;
    }
    
    public double getLength() {
        return length;
    }
    
    public double getEstimatedTime() {
        return estimatedTime;
    }
    
    public TrackSection getParentSection() {
        return parentSection;
    }

    public LaneSection getLeft() {
        return left;
    }

    public void setLeft(LaneSection left) {
        this.left = left;
    }

    public LaneSection getRight() {
        return right;
    }

    public void setRight(LaneSection right) {
        this.right = right;
    }

    public LaneSection getStraight() {
        return straight;
    }

    public void setStraight(LaneSection straight) {
        this.straight = straight;
    }
    
    public void setLaneIndex(int index) {
        this.laneIndex = index;
    }
    
    public int getLaneIndex() {
        return laneIndex;
    }
    
    public String toString() {
        return "LaneSection: length=" + length + " left exists=" + (left != null) + " right exists=" + (right != null) + " straight exists=" + (straight!=null); 
    }
}
