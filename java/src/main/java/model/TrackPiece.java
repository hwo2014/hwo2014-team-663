package model;

public class TrackPiece {
    
    private LanePiece[] lanes;
    private boolean isSwitch, isCurve = false;
    private int index;
    
    public TrackPiece(LanePiece... lanes) {
        this(-1, false, false, lanes);
    }
    
    public TrackPiece(int index, boolean isSwitch, boolean isCurve, LanePiece... lanes) {
        this.isSwitch = isSwitch;
        this.lanes = lanes;
        this.isCurve = isCurve;
        this.index = index;
    }
    
    public boolean isSwitch() {
        return isSwitch;
    }
    
    public boolean isCurve() {
        return isCurve;
    }
    
    public LanePiece[] getLanePieces() {
        return lanes;
    }
    
    public LanePiece getLanePiece(int index) {
        return lanes[index];
    }
    
    public void setLanePieces(LanePiece[] pieces) {
        lanes = pieces;
    }
    
    public int getIndex() {
        return index;
    }

    public String toString() {
        return "TrackPiece index=" + index + " isSwitch=" + isSwitch + " isCurve=" + isCurve;
    }
}
