package message.gameinit;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import noobbot.TrackBuilder;
import model.LanePiece;
import model.Track;
import model.TrackPiece;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GameInitMessage {
    
    public RaceData race;
    
    public static void main(String[] args) throws IOException {
        
        Path path = FileSystems.getDefault().getPath("json.txt");
        String json = new String(Files.readAllBytes(path));

        GsonBuilder gsonBuilder = new GsonBuilder();
        FieldNamingStrategy fieldNamingStrategy = new CustomFieldNamingStrategy();
        gsonBuilder.setFieldNamingStrategy(fieldNamingStrategy);
        Gson gson = gsonBuilder.create();
        GameInitMessage data = gson.fromJson(json, GameInitMessage.class);
        
        Track track = TrackBuilder.buildTrack(data.race.track);
        
        for(TrackPiece tp : track.getTrackPieces()) {
            for(LanePiece lp : tp.getLanePieces()) {
                System.out.println(lp.getLength());
            }
        }
    }
}

class CustomFieldNamingStrategy implements FieldNamingStrategy {
    @Override
    public String translateName(Field field) {
        
        if("isSwitch".equals(field.getName())) {
            return "switch";
        }
        
        return field.getName();
    }
}
