package message.gameinit;


public class TrackData {
    
    public String id, name;
    public PieceData[] pieces;
    public LaneData[] lanes;
    
}
